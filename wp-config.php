<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'newport-in-bloom');

/** MySQL database username */
define('DB_USER', 'newport');

/** MySQL database password */
define('DB_PASSWORD', 'hG6kApPIyFNPuTuV');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'aZsOB3rr?b _k7IGwo=;l]W:P#O;Y1mkg,$N:CL0`Wk&qV`l|GObD3v){}nY*hZG');
define('SECURE_AUTH_KEY',  'B=$Nu_S>}kR&8{uc5Xe;H{n7I)?O@NdHW&{pK(Y?(Xgc&<ev|6_GE!OgAFF(Wk;s');
define('LOGGED_IN_KEY',    '5cf>v#_H)(Pc;ShH0=B-AJ pE,kUm9C^>nEj&3@$eN1Jc84#.zu~TXYMjom!=Pjc');
define('NONCE_KEY',        'S-+#/V<J9qzvn9bF&d-AV6$gl8#SOhx)[4!]di1;a$22{JaSV-;p><C*P7#xppBb');
define('AUTH_SALT',        'JNIyd,j,MH~md*Cb1a-H;bEXIu/Pamcp}+$#T<B9f)}kN@?J_Fq.{._3P_mT.Y$2');
define('SECURE_AUTH_SALT', 'c@EHJcb8<xWncWg{5wK|@ZDKqaoE~w  F8V<Fim,65/8Ny;f4i?cC]5IPn-h60pO');
define('LOGGED_IN_SALT',   's=_y*vk).d`||A> N@7`O{q|WqSw]f.#@[USSugh8 ySCuc*bxCIdN<U?.l&jnJB');
define('NONCE_SALT',       'Hb<F~50.f331+CG@>m=ne+^NwjauSc2x&JT#aE9ii8vwWB(Mp9wV!VilFX>,!r!A');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
