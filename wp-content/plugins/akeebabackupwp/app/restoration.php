<?php
defined('_AKEEBA_RESTORATION') or die();
$restoration_setup = array(
	'kickstart.security.password' => '1a95hCVgYiIFvHmtWWc70FnBAjnIl8Qm2WCPEsjMowY=',
	'kickstart.tuning.max_exec_time' => '5',
	'kickstart.tuning.run_time_bias' => '75',
	'kickstart.tuning.min_exec_time' => '0',
	'kickstart.procengine' => 'direct',
	'kickstart.setup.sourcefile' => '/home/vagrant/Code/newport-in-bloom/wp-content/plugins/akeebabackupwp//app/tmp/update.zip',
	'kickstart.setup.destdir' => '/home/vagrant/Code/newport-in-bloom/wp-content/plugins/akeebabackupwp',
	'kickstart.setup.restoreperms' => '0',
	'kickstart.setup.filetype' => 'zip',
	'kickstart.setup.dryrun' => '0',
	'kickstart.setup.removepath' => 'akeebabackupwp',
);